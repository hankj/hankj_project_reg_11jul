// ##############################################
// #### Web Server for registration "regApp" 
// ##############################################

// load express module
var express = require("express");

//create an instance of express and body-parser
var regApp = express();
var bodyParser = require("body-parser");

//specify body-parser decoder 
regApp.use(bodyParser.urlencoded({"extended": "true"}));
regApp.use(bodyParser.json());

// Listen to Registration client here
regApp.post("/register", function(req, res){
    console.info(req.body.params);
    var regBlock = JSON.parse(req.body.params.registration);

    // print out received json block fields
    console.info("username: %s, email: %s, password: %s, gender: %s, dob: %s, address: %s, country: %s, contact: %d", 
        regBlock.username, regBlock.email, regBlock.password, regBlock.gender, regBlock.dob, regBlock.address, 
        regBlock.country, regBlock.contact);
    res.status(200).end();
});

regApp.get("/thankyou", function(req, res) {
    res.redirect("thankyou.html");
});

// look for static files from "public" and "public/bower_components" directories
regApp.use(express.static(__dirname + '/public'));
regApp.use(express.static(__dirname + '/public/bower_components'));


// Process error messages redirecting client to display an error.html file
// regApp.use(function(req, res){
//     console.info("Encounters error.");
//     res.redirect("error.html");
// });

// set port from command line argument, environment variable or default to 3000
regApp.set("port", process.argv[2] || process.APP_PORT || 5000);

// fire port to listen on port for client requests
regApp.listen( regApp.get("port"), function (){
   console.info ("Registration web server started on port %d",regApp.get("port")); 
});